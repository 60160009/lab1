const chai = require('chai');
const expect = chai.expect;
const math = require('./math')

describe('Test chai',()=>{
    it('should compare thing by expect ',()=>{
        expect(1).to.equal(1);
    })
    it('should compare another things by expect',()=>{
        expect(5>8).to.be.false;
        expect({name:'lookkaew'}).to.deep.equal({name:'lookkaew'});
        expect({name:'lookkaew'}).to.have.property('name').to.equal('lookkaew') ;
        expect({}).to.be.a('object');
        expect('lookkaew').to.be.a('string')
        expect('lookkaew'.length).to.equal(8);
        expect('lookkaew').to.lengthOf(8);
        expect([1,2,3]).to.lengthOf(3); 
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist
    })
});
describe("Math module",() =>{
    context('Function add1',()=>{
        it('ควรส่งค่ากลับเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number')
        })
        it('Add(1,1) ควรส่งค่ากลับเป็น 2',()=>{
            expect(math.add1(1,1)).to.equal(2);
        })
    })
})
